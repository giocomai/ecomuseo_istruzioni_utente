# Manuale utente per l'archivio digitale dell'Ecomuseo della Valle dei Laghi

Istruzioni per l'archivio digitale dell'Ecomuseo della Valle dei Laghi (Trentino).

Questo repository contiene i file necessari per genereare il manuale in vari formati (html, pdf, epub). Questo materiale è realizzato con [bookdown](https://bookdown.org/).

Un link diretto al manuale verrà reso pubblico quando l'archivio sarà effettivamente online. 

# Licenza

[CC-BY](https://creativecommons.org/licenses/by/4.0/deed.it)